# DAIRYDB versions building scripts.


## First files

```bash
#Table Corrections

#ligne 10439 décalage d'une cellule
#ligne 2341 décalages

# Upper case all sequences
# paste DAIRYdb_v1.2.4_20200221.csv <(cut -f 21 DAIRYdb_v1.2.4_20200221.csv|tr '[a-z]' '[A-Z]') > DAIRYdb_v1.2.4_20200221_OK.csv


#Fasta
sed "1d" DAIRYdb_v1.2.4_20200221_OK.csv |awk -F"\t" '{print ">" $20 "\n" $22}' > DDB_sintax.fa
sed "1d" DAIRYdb_v1.2.4_20200221_OK.csv |awk -F"\t" '{print ">" $6 "\n" $22}' > DDB_qiime.fa

#Taxo
sed "1d" DAIRYdb_v1.2.4_20200221_OK.csv |awk -F"\t" '{print $6 "\tk__" $10 "; p__" $11 "; c__" $12 "; o__" $13 "; f__" $14 "; g__" $15 "; s__" $17}' > DDB.tax
```

## QIIME
```bash

BASEN=DAIRYdb_v1.2.4_20200603

#Qiime
conda activate qiime2-2020.2

qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path DDB_qiime.fa \
  --output-path ${BASEN}.qza

qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path DDB.tax \
  --output-path ${BASEN}_taxonomy.qza

qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads ${BASEN}.qza \
  --i-reference-taxonomy ${BASEN}_taxonomy.qza \
  --o-classifier ${BASEN}_classifier.qza

conda deactivate
```

## Usearch sintax
```bash
usearch11 -makeudb_usearch DDB_sintax.fa -output ${BASEN}.udb -dbstep 1
```

## Blast
```bash
sed "s/ /_/g" DDB_sintax.fa|sed "s/tax=//g" > ${BASEN}_blast.fasta
makeblastdb -in ../${BASEN}_blast.fasta -dbtype nucl
```


## Metaxa2.2 dbb
```bash
DDBtable=DAIRYdb_v1.2.4_20200221_OK.csv
grep -P "\.B\tBacteria" $DDBtable | awk -F"\t" '{print ">" $4 " - "  $19 "\n" $22}'  > ${BASEN}_metaxa_Bacteria.fa

grep -P "\.A\tArchaea" $DDBtable | awk -F"\t" '{print ">" $4 " - "  $19 "\n" $22}'  > ${BASEN}_metaxa_Archaea.fa

sed -e '1d' $DDBtable |awk -F"\t" '{print ">" $4 "\t" $19}' > ${BASEN}_metaxa.tax

metaxa2_dbb -o SSU_${BASEN}_MTX -g SSU_${BASEN}_MTX -t ${BASEN}_metaxa.tax --auto_rep T --cpu 6 --cutoffs 0,75,78.5,82,86.5,94.5,98.65 --save_raw T -a ${BASEN}_metaxa_Archaea.fa -b ${BASEN}_metaxa_Bacteria.fa --filter_uncultured F --correct_taxonomy F --evaluate F --plus T --divergent T
```

## KRAKEN2
```bash
mkdir ddb_kraken ddb_kraken/data ddb_kraken/library ddb_kraken/taxonomy;

cd ddb_kraken
sed 1d ~/bank/DAIRYdb_v1.2.0/20190222/DAIRYdb_v1.2.0_20190225.csv| awk -F'\t' '{print $1 "\t k__" $10 "; p__" $11 "; c__" $12 "; o__" $13 "; f__" $14 "; g__" $15 "; s__" $17}' > data/ddb_taxonomy.txt
sed 1d ~/bank/DAIRYdb_v1.2.0/20190222/DAIRYdb_v1.2.0_20190225.csv| awk -F'\t' '{print ">" $1 "\n" $21}' > library/ddb_seq.fna

/opt/kraken2/build_gg_taxonomy.pl data/ddb_taxonomy.txt
mv *dmp taxonomy;

/opt/kraken2/kraken2-build --db . --build --threads 3
```

## Mothur
```bash
cat ../DAIRYdb_v1.2.4_20200221_OK.csv | awk -F'\t' '{print $6 "\t" $18 ";"}' | sed "1d" > mothur_DDB_1.2.4_taxonomy.txt
cat ../DAIRYdb_v1.2.4_20200221_OK.csv | awk -F'\t' '{print ">" $6 "\n" $22 ";"}' | sed "1,2d" > mothur_DDB_1.2.4_sequences.fasta
```

### Tests
```bash
/opt/kraken2/kraken2 --db . ~/Documents/Projets/2019/these_JP/16S_dispo1/dada2_out/rep-seqs.fna --use-names --report test_ddb.report> test_res_JP_ddb.csv

/opt/Mothur.Ubuntu_18/mothur/mothur "#classify.seqs(fasta=testV3V4.fasta, template=../mothur/mothur_DDB_1.2.4_sequences.fasta , taxonomy=../mothur/mothur_DDB_1.2.4_taxonomy.txt)"

DBudb=~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/sintax/DAIRYdb_v1.2.4_20200603.udb
usearch11 -sintax testV3V4.fasta -db $DBudb -tabbedout out.sintax -strand both -sintax_cutoff 0.6

DBmet=~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/SSU_DAIRYdb_v1.2.4_20200603_MTX/blast
metaxa2 -i testV3V4.fasta -g ssu -o out_metaxa2 --cpu 4 --taxonomy T --plus T -T 0,75,78.5,82,86.5,94.5,98.65 -taxlevel 7 -d $DBmet -t b,a

DBblast=~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/blast/DAIRYdb_v1.2.4_20200603_blast.fasta
blastn -query testV3V4.fasta -db $DBblast -num_threads 5 -out OUT_tax_blast.txt -evalue 1 -outfmt 6 -perc_identity 97 -max_target_seqs 50

DBqiime=~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/qiime/DAIRYdb_v1.2.4_20200603_classifier.qza

qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path testV3V4.fasta \
  --output-path testV3V4.qza

qiime feature-classifier classify-sklearn \
  --i-reads testV3V4.qza \
  --i-classifier $DBqiime \
  --o-classification out_qiimetest
```


## IDTAXA

```bash
sed "1d" DAIRYdb_v1.2.4_20200221_OK.csv |awk -F"\t" '{print ">" $6 "\n" $22}' > DDB_idtaxa.fa
sed -i "s,\[DHVEG-6\],DHVEG-6,g" DDB_idtaxa.fa


sed "1d" ../DAIRYdb_v1.2.4_20200221_OK.csv |awk -F"\t" '{print $6 "\tk__" $10 "\tp__" $11 "\tc__" $12 "\to__" $13 "\tf__" $14 "\tg__" $15 "\ts__" $17}' > DDB_idtaxa.tax
sed -i "s,\[DHVEG-6\],DHVEG-6,g" DDB_idtaxa.tax

```

```r
library(ranomaly)   #devtools::load_all("~/Repository/LRF/ranomaly/")
# IDTAXA database formatting
setwd("~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/idtaxa")

taxtable <- read.table("DDB_idtaxa.tax", sep="\t", stringsAsFactors=FALSE, h = FALSE)
taxtable[taxtable == ""] = NA
row.names(taxtable) = taxtable[,1]	#rownames must be unique
ttable_ok = taxtable[,-1]

#Fill tax table
filltable = fill_tax_fun(ttable_ok, prefix = FALSE)
# #Check tax table
check1 = check_tax_fun(filltable, output = NULL)

#Generate taxid
taxid <- taxid_fun(taxtable = check1, output = NULL)
head(taxid[taxid$Level == 1,])


#Train IDTAXA db
idtaxa_traindb(taxtable = check1, taxid = taxid, seqs = "DDB_idtaxa.fa", prunedb = NULL, outputDIR = "./", outputDBname = "DAIRYdb_v1.2.4_20200603_IDTAXA.rdata", returnval = FALSE)

# Test
idtaxa_assign_fun("../../tests/testV3V4.fasta", "DAIRYdb_v1.2.4_20200603_IDTAXA.rdata")
```

## FROGS
```bash
#FROGS on genotoul.
# /save/frogs/src/FROGS_prod
#python2.7 /save/frogs/how_to_databank/fasta2RDP.py -t frogs_$TAXO -d $FASTA2 --rdp-taxonomy ${BASEN$

#En local
BASEN=DAIRYdb_v1.2.4_20200603

frogs_dir=~/Repository/FROGS #/save/frogs/src/FROGS_prod
# add modules
export PATH=$frogs_dir/libexec:$frogs_dir/app:$PATH
export PYTHONPATH=$frogs_dir/lib:$PYTHONPATH

python2.7 ~/Bureau/fasta2RDP.py -t DDB.tax -d DDB_qiime.fa --rdp-taxonomy ${BASEN}_frogs.tax --rdp-fasta ${BASEN}_frogs.fasta

makeblastdb -in ${BASEN}_frogs.fasta -dbtype nucl

#~/bank/DAIRYdb_v1.2.0/20200603/DAIRYdb_v1.2.4/versions/frogs
#tar -czvf ${BASEN}_frogs.tgz ${BASEN}_frogs*
```


## Diff 1.2.0 (10389) 1.2.4 (10439)
```bash
diff -u ~/bank/DAIRYdb_v1.2.0/20190222/DAIRYdb_v1.2.0.tax DDB.tax|grep -E -i "^\+[a-z]" > DDBv1.2.4_changes.txt

diff -u ~/bank/DAIRYdb_v1.2.0/20190222/DAIRYdb_v1.2.0.tax DDB.tax|grep -E -i "^\-[a-z]" >> DDBv1.2.4_changes.txt

diff -c ~/bank/DAIRYdb_v1.2.0/20190222/DAIRYdb_v1.2.0.tax DDB.tax|grep -E -i "^\! [a-z]" >> DDBv1.2.4_changes.txt
```
